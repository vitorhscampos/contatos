package com.example.indb.appcontatos.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.indb.appcontatos.R;
import com.example.indb.appcontatos.SessionManagement;
import com.example.indb.appcontatos.models.Contact;
import com.example.indb.appcontatos.models.Group;
import com.example.indb.appcontatos.models.User;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class MainActivity extends AppCompatActivity {

    private Realm realm;
    public static String EXTRA_NOME;
    private ArrayList<String> listdata = new ArrayList<>();

    @BindView(R.id.new_contact)
    FloatingActionButton button;
    @BindView(R.id.listView)
    ListView lista;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        ButterKnife.bind(this);

        int userId = User.loggedUserId;
        if(userId == 0) {
            User.loggedUserId = new SessionManagement(this).getUserId();
        }

        realm = Realm.getDefaultInstance();
        getContacts();

    }

    @Override
    protected void onResume() {
        super.onResume();
        getContacts();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.atualizar:
                listdata.clear();
                getContacts();
                return true;
            case R.id.novo_grupo:
                Intent groupIntent = new Intent(getApplicationContext(), GroupsActivity.class);
                startActivity(groupIntent);
                return true;
            case R.id.sair:
                new SessionManagement(this).logOut();
                Intent authIntent = new Intent(getApplicationContext(), AuthActivity.class);
                startActivity(authIntent);
                finish();
                return true;
            case R.id.filtrar:
                filterDialog();
            default:
                return true;
        }

    }

    @OnClick(R.id.new_contact)
    public void newContact(){
        Intent intent = new Intent(getApplicationContext(), NewContactActivity.class);
        startActivity(intent);
    }

    @SuppressLint("RestrictedApi")
    private void filterDialog(){
        ArrayList<String> groupList = new ArrayList<>();
        groupList.add("Todos os grupos");

        RealmResults results = realm.where(Group.class).findAll();

        final ListView all = new ListView(this);

        for(int i = 0; i < results.size(); i++) {
            Group group = (Group) results.get(i);
            assert group != null;
            groupList.add(group.getName());
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(
                getApplicationContext(),
                android.R.layout.simple_list_item_1,
                groupList);


        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Exibir somente do grupo: ");
        all.setAdapter(arrayAdapter);
        builder.setView(all, 0, 20, 0, 0);
        final AlertDialog dialog = builder.show();

        all.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String nome = (String) all.getItemAtPosition(i);
                if (!nome.equals("Todos os grupos")){
                    getContactsGroup(nome);
                    dialog.dismiss();
                }else{
                    getContacts();
                    dialog.dismiss();
                }
            }
        });


    }

    private void getContacts(){
        listdata.clear();

        RealmResults results = realm.where(Contact.class)
                .equalTo("user_id", User.loggedUserId)
                .sort("name", Sort.ASCENDING)
                .findAll();

        for (int i = 0; i < results.size(); i++) {
            Contact contact = (Contact) results.get(i);
            assert contact != null;
            listdata.add(contact.getName());
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(
                getApplicationContext(),
                android.R.layout.simple_list_item_1,
                listdata);

        lista.setAdapter(arrayAdapter);
        lista.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String nome = (String) lista.getItemAtPosition(position);
                Intent intent = new Intent(getApplicationContext(), ContactActivity.class);

                intent.putExtra(EXTRA_NOME, nome);
                startActivity(intent);
            }
        });
    }

    private void getContactsGroup(String groupName){
        listdata.clear();
        int groupId = Group.getGroup(groupName).getId();
        RealmResults results = realm.where(Contact.class)
                .equalTo("user_id", User.loggedUserId)
                .equalTo("group_id", groupId)
                .sort("name", Sort.ASCENDING)
                .findAll();

        for (int i = 0; i < results.size(); i++) {
            Contact contact = (Contact) results.get(i);
            assert contact != null;
            listdata.add(contact.getName());
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(
                getApplicationContext(),
                android.R.layout.simple_list_item_1,
                listdata);

        lista.setAdapter(arrayAdapter);
        lista.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String nome = (String) lista.getItemAtPosition(position);
                Intent intent = new Intent(getApplicationContext(), ContactActivity.class);

                intent.putExtra(EXTRA_NOME, nome);
                startActivity(intent);
            }
        });
    }
}