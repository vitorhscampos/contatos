package com.example.indb.appcontatos.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.indb.appcontatos.R;
import com.example.indb.appcontatos.SessionManagement;
import com.example.indb.appcontatos.models.User;

import org.mindrot.jbcrypt.BCrypt;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;

public class AuthActivity extends AppCompatActivity {

    @BindView(R.id.txtEmail)
    EditText txtEmail;
    @BindView(R.id.txtSenha)
    EditText txtSenha;
    @BindView(R.id.btnLogin)
    Button btnLogin;
    @BindView(R.id.btnRegister)
    Button btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        assert ab != null;

        ButterKnife.bind(this);
    }

    @OnClick(R.id.btnRegister)
    public void register(){
        Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btnLogin)
    public void login() {
        String senha = txtSenha.getText().toString();
        String email = txtEmail.getText().toString();

        Realm realm = Realm.getDefaultInstance();

        User user = realm.where(User.class).equalTo("email", email).findFirst();

        if (user != null && checkPassword(senha, user.getPassword())) {
            User.loggedUserId = user.getId();
            new SessionManagement(this).logIn();
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
        } else {
            txtSenha.getText().clear();
            txtEmail.setError(getString(R.string.login_error));
            txtSenha.setError(getString(R.string.login_error));
        }
    }

    private boolean checkPassword(String password, String hash) {
        return BCrypt.checkpw(password, hash);
    }
}