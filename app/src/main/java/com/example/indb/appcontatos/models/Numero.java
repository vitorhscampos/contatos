package com.example.indb.appcontatos.models;

import io.realm.RealmObject;

public class Numero extends RealmObject {

    private int contact_id;
    private String number;

    public int getContact_id() {
        return contact_id;
    }

    public void setContact_id(int contact_id) {
        this.contact_id = contact_id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }


}
