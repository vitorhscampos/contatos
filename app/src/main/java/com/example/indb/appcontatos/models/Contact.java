package com.example.indb.appcontatos.models;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class Contact extends RealmObject{

    @PrimaryKey
    private int id;

    private int user_id;

    private int group_id;
    @Required
    private String email;
    @Required
    private String name;
    @Required
    private String img_path;

    private Numero numero;

    public Numero getNumero() {
        return numero;
    }

    public void setNumero(Numero numero) {
        this.numero = numero;
    }

    public String getImg_path() {
        return img_path;
    }

    public void setImg_path(String img_path) {
        this.img_path = img_path;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getGroup_id() {
        return group_id;
    }

    public void setGroup_id(int group_id) {
        this.group_id = group_id;
    }

    public int getId() {
        return id;
    }

    public static int getNewId() {

        Realm realm = Realm.getDefaultInstance();

        Number lastId = realm.where(Contact.class).max("id");
        int nextId;
        if(lastId == null) {
            nextId = 1;
        } else {
            nextId = lastId.intValue() + 1;
        }

        return nextId;
    }

}
