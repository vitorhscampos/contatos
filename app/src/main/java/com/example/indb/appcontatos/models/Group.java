package com.example.indb.appcontatos.models;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Group extends RealmObject {

    @PrimaryKey
    private int id;

    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static int getNewId() {

        Realm realm = Realm.getDefaultInstance();

        Number lastId = realm.where(Group.class).max("id");
        int nextId;
        if(lastId == null) {
            nextId = 1;
        } else {
            nextId = lastId.intValue() + 1;
        }

        return nextId;
    }

    public static Group getGroup(String name){
        Realm realm = Realm.getDefaultInstance();

        return realm.where(Group.class).equalTo("name", name).findFirst();
    }
}
