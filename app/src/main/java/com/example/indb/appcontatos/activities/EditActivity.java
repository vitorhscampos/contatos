package com.example.indb.appcontatos.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.indb.appcontatos.R;
import com.example.indb.appcontatos.models.Contact;
import com.example.indb.appcontatos.models.Group;
import com.example.indb.appcontatos.models.Numero;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;

public class EditActivity extends AppCompatActivity {

    @BindView(R.id.btnSalvar)
    Button button;
    @BindView(R.id.txtNewName)
    EditText txtName;
    @BindView(R.id.txtNewEmail)
    EditText txtEmail;
    @BindView(R.id.txtNewNumber)
    EditText txtNumber;
    @BindView(R.id.grupos)
    Spinner spinner;

    private int erro;
    private Realm realm;
    private Contact contact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        assert ab != null;
        ab.setDisplayHomeAsUpEnabled(true);

        realm = Realm.getDefaultInstance();
        ButterKnife.bind(this);

        contact = realm.where(Contact.class).equalTo("id", ContactActivity.id).findFirst();
        populate();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    private void populate() {

        txtNumber.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        txtName.setText(contact.getName());
        txtEmail.setText(contact.getEmail());
        txtNumber.setText(contact.getNumero().getNumber());

        //Spinner
        int groupId = contact.getGroup_id();
        String groupName = null;
        ArrayList<String> listdata = new ArrayList<>();

        RealmResults results = realm.where(Group.class).findAll();
        for (int i = 0; i < results.size(); i++) {
            Group group = (Group) results.get(i);
            if (groupId == (group != null ? group.getId() : 0)) {
                groupName = group != null ? group.getName() : null;
            }
            assert group != null;
            listdata.add(group.getName());
        }

        ArrayAdapter arrayAdapter = new ArrayAdapter<>(
                getApplicationContext(),
                android.R.layout.simple_spinner_dropdown_item,
                listdata
        );

        int spinnerPosition = arrayAdapter.getPosition(groupName);

        spinner.setAdapter(arrayAdapter);
        spinner.setSelection(spinnerPosition);

    }

    @OnClick(R.id.btnSalvar)
    public void save() {

        final String name = txtName.getText().toString();
        final String email = txtEmail.getText().toString();
        final String number = txtNumber.getText().toString();
        final String grupo = spinner.getSelectedItem().toString();

        Group group = realm.where(Group.class).equalTo("name", grupo).findFirst();

        final int group_id = group != null ? group.getId() : 0;

        if (validate(name, email, number, group_id)) {

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(@NonNull Realm realm) {
                    contact.setName(name);
                    contact.setEmail(email);
                    contact.setGroup_id(group_id);
                    Numero numero = contact.getNumero();
                    numero.setNumber(number);
                    contact.setNumero(numero);
                    realm.copyToRealmOrUpdate(contact);
                }
            });
            finish();
            Toast.makeText(getApplicationContext(), R.string.saved,
                    Toast.LENGTH_LONG).show();
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setPositiveButton(R.string.try_again, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });

            if (erro == 1) {
                builder.setMessage(R.string.all_fields_required);
            } else if (erro == 2) {
                builder.setMessage(R.string.invalid_email);
            }

            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    private boolean validate(String name, String email, String number, int group_id) {
        if (name.isEmpty() || number.isEmpty() || group_id == 0) {
            erro = 1;
            return false;
        } else if (!isValidEmail(email)) {
            erro = 2;
            return false;
        }
        return true;
    }

    private boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }
}