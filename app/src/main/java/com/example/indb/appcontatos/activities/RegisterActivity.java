package com.example.indb.appcontatos.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.indb.appcontatos.R;
import com.example.indb.appcontatos.models.User;

import org.mindrot.jbcrypt.BCrypt;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;

public class RegisterActivity extends AppCompatActivity {

    private Realm realm;

    @BindView(R.id.btnRegister)
    Button btnRegister;
    @BindView(R.id.txtNome)
    EditText txtName;
    @BindView(R.id.txtEmail)
    EditText txtEmail;
    @BindView(R.id.txtSenha)
    EditText txtPassword;
    @BindView(R.id.txtSenha2)
    EditText txtPasswordConf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        assert ab != null;
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setTitle(R.string.register_user);

        realm = Realm.getDefaultInstance();

        ButterKnife.bind(this);
    }

    @OnClick(R.id.btnRegister)
    public void validate() {
        String name = txtName.getText().toString();
        String email = txtEmail.getText().toString();
        String password = txtPassword.getText().toString();
        String passwordConf = txtPasswordConf.getText().toString();

        if (name.isEmpty()) {
            txtName.setError(getString(R.string.field_required));
            txtName.requestFocus();
        }else if (!isValidEmail(email)) {
            txtEmail.getText().clear();
            txtEmail.setError(getString(R.string.invalid_email));
            txtEmail.requestFocus();
        }else if (!password.equals(passwordConf) || password.isEmpty() || passwordConf.isEmpty()) {
            txtPassword.getText().clear();
            txtPasswordConf.getText().clear();
            txtPassword.setError(getString(R.string.pass_conf_error));
            txtPassword.requestFocus();
        }else{
            save(name, email, password);
        }
    }

    private void save(final String name, final String email, final String password) {

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(@NonNull Realm realm) {
                User newUser = realm.createObject(User.class, User.getNewId());
                newUser.setName(name);
                newUser.setEmail(email);
                newUser.setPassword(BCrypt.hashpw(password, BCrypt.gensalt()));
            }
        });

        finish();
        Toast.makeText(getApplicationContext(), R.string.user_created,
                Toast.LENGTH_LONG).show();

    }

    private boolean isValidEmail(CharSequence email) {
        return (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches());
    }
}
