package com.example.indb.appcontatos.models;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class User extends RealmObject {

    @PrimaryKey
    private int id;

    private String name;
    private String email;
    private String password;

    public static int loggedUserId;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return this.password;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static int getNewId() {

        Realm realm = Realm.getDefaultInstance();

        Number lastId = realm.where(User.class).max("id");
        int nextId;
        if(lastId == null) {
            nextId = 1;
        } else {
            nextId = lastId.intValue() + 1;
        }

        return nextId;
    }
}
