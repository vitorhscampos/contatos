package com.example.indb.appcontatos.activities;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.indb.appcontatos.R;
import com.example.indb.appcontatos.models.Group;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;

public class GroupsActivity extends AppCompatActivity {

    @BindView(R.id.groupsList)
    ListView listView;
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_groups);
        Toolbar myToolbar = findViewById(R.id.toolbar);
        myToolbar.setTitle(R.string.groups);
        setSupportActionBar(myToolbar);
        ActionBar ab = getSupportActionBar();
        assert ab != null;
        ab.setDisplayHomeAsUpEnabled(true);

        realm = Realm.getDefaultInstance();

        ButterKnife.bind(this);

        getAllGroups();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.groups_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.new_groups:
                groupDialog();
                return true;
            default:
                finish();
                return true;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();

    }

    public void getAllGroups() {
        ArrayList<String> listdata = new ArrayList<>();
        RealmResults results = realm.where(Group.class).findAll();

        for (int i = 0; i < results.size(); i++) {
            Group group = (Group) results.get(i);
            assert group != null;
            if (!(group.getName().equals("Sem Grupo"))) {
                listdata.add(group.getName());
            }
        }
        setData(listdata);
    }

    public void setData(ArrayList<String> data) {

        ArrayAdapter arrayAdapter = new ArrayAdapter<>(
                getApplicationContext(),
                android.R.layout.simple_list_item_1,
                data
        );

        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String nome = (String) listView.getItemAtPosition(position);
                editDialog(nome);
            }
        });
    }

    @SuppressLint("RestrictedApi")
    private void groupDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        final EditText group_name = new EditText(this);
        group_name.setHint(R.string.group_name);

        builder.setTitle(R.string.new_group);

        builder.setView(group_name, 50, 0, 50, 0);

        builder.setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String group = group_name.getText().toString();
                newGroup(group);

            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });

        builder.show();

    }

    private void newGroup(final String group_name) {

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(@NonNull Realm realm) {
                Group newGroup = realm.createObject(Group.class, Group.getNewId());
                newGroup.setName(group_name);
            }
        });

        getAllGroups();
    }

    @SuppressLint("RestrictedApi")
    private void editDialog(final String nome) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        final EditText group_name = new EditText(this);
        group_name.setText(nome);

        builder.setTitle(R.string.edit_group);

        builder.setView(group_name, 50, 0, 50, 0);

        builder.setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String newName = group_name.getText().toString();
                update(nome, newName);
            }
        });

        builder.setNeutralButton(R.string.delete_group, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                deleteGroup(nome);
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });

        builder.show();
    }

    private void deleteGroup(String name) {
        final Group group = realm.where(Group.class).equalTo("name", name).findFirst();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(@NonNull Realm realm) {
                assert group != null;
                group.deleteFromRealm();
            }
        });
        Toast.makeText(getApplicationContext(), R.string.group_deleted,
                Toast.LENGTH_LONG).show();
        getAllGroups();
    }

    private void update(final String nome, final String newName) {
        final Group group = realm.where(Group.class).equalTo("name", nome).findFirst();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(@NonNull Realm realm) {
                assert group != null;
                group.setName(newName);
                realm.copyToRealmOrUpdate(group);
            }
        });
        Toast.makeText(getApplicationContext(), R.string.group_updated,
                Toast.LENGTH_LONG).show();
        getAllGroups();
    }

}
