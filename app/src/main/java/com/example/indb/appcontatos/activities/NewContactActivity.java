package com.example.indb.appcontatos.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.indb.appcontatos.R;
import com.example.indb.appcontatos.models.Contact;
import com.example.indb.appcontatos.models.Group;
import com.example.indb.appcontatos.models.Numero;
import com.example.indb.appcontatos.models.User;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;

public class NewContactActivity extends AppCompatActivity {

    private int erro; //1 = campos vazios, 2 = email inválido
    private String name;
    private Realm realm;
    private String email;
    private String grupo;
    private String number;
    private Integer group_id;

    @BindView(R.id.grupos)
    Spinner spnGrupos;
    @BindView(R.id.btnSalvar)
    Button button;
    @BindView(R.id.txtNewName)
    EditText txtName;
    @BindView(R.id.txtNewEmail)
    EditText txtEmail;
    @BindView(R.id.txtNewNumber)
    EditText txtNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_contact);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        assert ab != null;
        ab.setDisplayHomeAsUpEnabled(true);

        realm = Realm.getDefaultInstance();
        ButterKnife.bind(this);

        txtNumber.addTextChangedListener(new PhoneNumberFormattingTextWatcher());

        populateSpinner(spnGrupos);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @OnClick(R.id.btnSalvar)
    public void newContact() {
        name = txtName.getText().toString();
        email = txtEmail.getText().toString();
        number = txtNumber.getText().toString();
        grupo = spnGrupos.getSelectedItem().toString();

        Group group = realm.where(Group.class).equalTo("name", grupo).findFirst();

        group_id = group != null ? group.getId() : 0;

        if (validate(name, email, number, group_id)) {

            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    Contact newContact = realm.createObject(Contact.class, Contact.getNewId());
                    newContact.setName(name);
                    newContact.setEmail(email);
                    newContact.setImg_path("img/default.jpg");
                    newContact.setGroup_id(group_id);
                    newContact.setUser_id(User.loggedUserId);

                    Numero newNumero = realm.createObject(Numero.class);
                    newNumero.setNumber(number);
                    newNumero.setContact_id(newContact.getId());

                    newContact.setNumero(newNumero);
                }
            });
            finish();
            Toast.makeText(getApplicationContext(), R.string.contact_saved,
                    Toast.LENGTH_LONG).show();
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setPositiveButton(R.string.try_again, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                }
            });

            if (erro == 1) {
                builder.setMessage(R.string.all_fields_required);
                AlertDialog dialog = builder.create();
                dialog.show();
            } else if (erro == 2) {
                txtEmail.setError(getString(R.string.invalid_email));
            }
        }
    }

    private boolean validate(String name, String email, String number, int group_id) {
        if (name.isEmpty() || number.isEmpty() || group_id == 0) {
            erro = 1;
            return false;
        } else if (!isValidEmail(email)) {
            erro = 2;
            return false;
        }
        return true;
    }

    private boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    private void populateSpinner(Spinner spinner) {

        ArrayList<String> listdata = new ArrayList<>();

        RealmResults results = realm.where(Group.class).findAll();
        for (int i = 0; i < results.size(); i++) {
            Group group = (Group) results.get(i);
            assert group != null;
            listdata.add(group.getName());
        }

        ArrayAdapter arrayAdapter = new ArrayAdapter<>(
                getApplicationContext(),
                android.R.layout.simple_spinner_dropdown_item,
                listdata
        );

        spinner.setAdapter(arrayAdapter);
    }
}
