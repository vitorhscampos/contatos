package com.example.indb.appcontatos.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.indb.appcontatos.CircleTransform;
import com.example.indb.appcontatos.R;
import com.example.indb.appcontatos.constants.Constants;
import com.example.indb.appcontatos.models.Contact;
import com.example.indb.appcontatos.models.Group;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;

public class ContactActivity extends AppCompatActivity {

    protected static Integer id;
    @BindView(R.id.btnExcluir)
    Button btnExcluir;
    @BindView(R.id.btnEditar)
    Button btnEditar;
    @BindView(R.id.lstNumeros)
    ListView lista;
    @BindView(R.id.txtNome)
    TextView txtNome;
    @BindView(R.id.pic)
    ImageView pic;
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        assert ab != null;
        ab.setDisplayHomeAsUpEnabled(true);

        realm = Realm.getDefaultInstance();
        Intent intent = getIntent();
        String nome = intent.getStringExtra(MainActivity.EXTRA_NOME);

        ButterKnife.bind(this);
        getContact(nome);

    }

    @OnClick(R.id.btnExcluir)
    public void showDeleteDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ContactActivity.this);

        builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                deleteContact(ContactActivity.id);
            }
        }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        builder.setMessage(R.string.confirm_delete);

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    private void getContact(String name) {
        Contact contact = realm.where(Contact.class).equalTo("name", name).findFirst();
        displayContact(contact);
    }

    private void displayContact(Contact contact) {
        ArrayList<String> listdata = new ArrayList<>();
        String name = contact.getName();
        String email = contact.getEmail();
        String img = contact.getImg_path();
        String numero = contact.getNumero().getNumber();
        int group = contact.getGroup_id();
        Group group1 = realm.where(Group.class).equalTo("id", group).findFirst();

        listdata.add(email);
        listdata.add(numero);
        listdata.add(group1 != null ? group1.getName() : null);
        id = contact.getId();

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(
                getApplicationContext(),
                android.R.layout.simple_list_item_1,
                listdata);

        lista.setAdapter(arrayAdapter);

        Picasso.with(getApplicationContext())
                .load(Constants.URL + img)
                .resize(320, 320)
                .transform(new CircleTransform())
                .into(pic);

        txtNome.setText(name);

        lista.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent;
                String item = (String) lista.getItemAtPosition(position);
                if (item.contains("@")) {
                    intent = new Intent(Intent.ACTION_SENDTO);
                    intent.setData(Uri.fromParts("mailto", item, null));
                    PackageManager packageManager = ContactActivity.this.getPackageManager();
                    if (intent.resolveActivity(packageManager) != null) {
                        startActivity(intent);
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.no_email_app,
                                Toast.LENGTH_LONG).show();
                    }
                } else {
                    intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + item));
                    startActivity(intent);
                }
            }
        });


    }

    private void deleteContact(Integer id) {

        final Contact contact = realm.where(Contact.class).equalTo("id", id).findFirst();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(@NonNull Realm realm) {
                assert contact != null;
                contact.deleteFromRealm();
            }
        });

        finish();
        Toast.makeText(getApplicationContext(), R.string.contato_excluido, Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.btnEditar)
    public void editar() {
        Intent intent = new Intent(getApplicationContext(), EditActivity.class);
        startActivity(intent);
        finish();
    }
}