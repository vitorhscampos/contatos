package com.example.indb.appcontatos;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.indb.appcontatos.models.User;

public class SessionManagement {

    private  SharedPreferences.Editor editor;
    private  SharedPreferences pref;

    public SessionManagement(Context context) {
        pref = context.getSharedPreferences("Pref", 0);
        editor = pref.edit();
    }

    public  void logIn(){
        editor.putBoolean("loggedIn", true);
        editor.putInt("userId", User.loggedUserId);
        editor.commit();
    }

    public  void logOut(){
        editor.remove("loggedIn");
        editor.remove("UserId");
        editor.commit();
    }

    public int getUserId() {
        return pref.getInt("userId", 0);
    }

    public boolean isLoggedIn(){
       return pref.getBoolean("loggedIn", false);
    }



}
